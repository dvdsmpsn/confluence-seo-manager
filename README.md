# Introduction #

This is the source for the SEO Manager plugin for Confluence: https://marketplace.atlassian.com/plugins/com.playsql.seo-manager

### Where can I submit issues? ###

Look in the left side-bar: There is a link '[Issues](https://bitbucket.org/aragot/confluence-seo-manager/issues)'. Submit your issues there.

**However...** if your question was "Can you fix issues for us?", please be aware that I'm busy with other projects and I don't take much time to fix issues for this one. The best way to get your issues fixed is to fix them yourself, this is the purpose of open-sourcing this codebase.

### How to contribute ###

* Fork,
* Make a pull request,
* Carefully explain your intent and be ready for some push back: I'm the one who publishes it to the Marketplace.
* In the meantime of course, you can use your own version on your servers.

### Is it possible to change the hideous plugin icon? ###

Oh great! Please include the new icon in the src/main/resources/img folder and I'll upload it next time.

### How do I develop? ###

* Download the repository,
* Run ``mvn amps:debug`` in the root directory,
* In a separate terminal, run ``mvn confluence:cli`` and type ``pi`` each time you want to compile and deploy.

### How do I release? ###

You don't release. Only the landlord of this repository can release it. Basically:

* He changes the pom.xml number to final numbers,
* He runs ```mvn install```,
* He saves the .jar in a separate folder,
* He uploads it onto the Atlassian Marketplace.

It may look very _artisanal_. It is. We should use ``mvn release:prepare/perform`` and we don't do it yet because we didn't setup a Maven repository yet.

Thank you!
package com.playsql.seo.web.admin;

import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.pages.*;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.DefaultPermissionManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SeoAdmin extends ConfluenceActionSupport {

    SpaceManager spaceManager;
    PageManager pageManager;
    ContentPermissionManager contentPermissionManager;
    PermissionManager permissionManager;
    SettingsManager settingsManager;

    private String robotstxt;
    private String sitemapxml;

    private String robotsFilePath;
    private String sitemapFilePath;

    private List<PageDef> urlList = Lists.newArrayList();

    private File webappRoot;
    {
        String tomcatRoot = System.getProperty("catalina.base");

        if (isWebappRoot(tomcatRoot)) {
            webappRoot = new File(tomcatRoot);
        } else if (isWebappRoot(tomcatRoot, "webapps")) {
            webappRoot = new File(tomcatRoot, "webapps");
        } else if (isWebappRoot(tomcatRoot, "webapps", "confluence")) {
            webappRoot = new File(new File(tomcatRoot, "webapps"), "confluence");
        } else if (isWebappRoot(tomcatRoot, "confluence")) {
            webappRoot = new File(tomcatRoot, "confluence");
        } else {
            webappRoot = new File(tomcatRoot, "install-dir");
        }
    }

    private static boolean isWebappRoot(String... subdirs) {
        File base = null;
        if (subdirs != null) for (String subdir : subdirs) {
            base = new File(base, subdir);
        }
        return new File(base, "login.vm").exists();
    }

    private File robots() {
        String presetLocation = System.getProperty("com.playsql.seo.locations.robots");
        File robotsFile;
        if (presetLocation != null) {
            robotsFile = new File(presetLocation);
        } else {
            robotsFile = new File(webappRoot, "robots.txt");
        }
        robotsFilePath = robotsFile.getAbsolutePath();
        return robotsFile;
    }

    private File sitemap() {
        String presetLocation = System.getProperty("com.playsql.seo.locations.sitemap");
        File sitemapFile;
        if (presetLocation != null) {
            sitemapFile = new File(presetLocation);
        } else {
            sitemapFile = new File(webappRoot, "sitemap.xml");
        }
        sitemapFilePath = sitemapFile.getAbsolutePath();
        return sitemapFile;
    }

    public String doDefault() {
        //((BootstrapManager) BootstrapUtils.getBootstrapManager())
        File robotsFile = robots();
        File sitemapFile = sitemap();

        FileInputStream robotsContent = null;
        try {
            robotsContent = new FileInputStream(robotsFile);
            robotstxt = IOUtils.toString(robotsContent);
        } catch (FileNotFoundException e) {
            robotsFilePath += " - Not Found";
        } catch (IOException e) {
            robotsFilePath += " - Can't read - " + e.getMessage();
        } finally {
            IOUtils.closeQuietly(robotsContent);
        }

        FileInputStream sitemapContent = null;
        try {
            sitemapContent = new FileInputStream(sitemapFile);
            sitemapxml = IOUtils.toString(sitemapContent);
        } catch (FileNotFoundException e) {
            sitemapFilePath += " - Not Found";
        } catch (IOException e) {
            sitemapFilePath += " - Can't read - " + e.getMessage();
        } finally {
            IOUtils.closeQuietly(sitemapContent);
        }

        return INPUT;
    }

    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public String doFill() {
        File robotsFile = robots();
        File sitemapFile = sitemap();
        addActionMessage(getText("seo.admin.is.a.suggestion"));

        String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();

        if (!permissionManager.hasPermission(null, Permission.VIEW, PermissionManager.TARGET_APPLICATION)) {
            addActionError(getText("seo.admin.not.public"));
            return INPUT;
        }

        for (Space space : spaceManager.getAllSpaces(SpacesQuery.newQuery().withPermission(SpacePermission.VIEWSPACE_PERMISSION).forUser(null).build())) {
            String spaceTitle = space.getDisplayTitle();
            urlList.add(new PageDef(spaceTitle, "Space " + space.getKey() + " (" + space.getDisplayTitle() + ")"));

            TreeBuilder treeBuilder = new TreeBuilder(null, contentPermissionManager, pageManager);
            ContentTree tree = treeBuilder.createPageTree(space);
            for (Page page : tree.getPages()) {
                if (page.getTitle().startsWith("_"))
                    continue;
                urlList.add(new PageDef(spaceTitle, page.getTitle(), baseUrl + page.getUrlPath(), page.getLastModificationDate(), "weekly", "1"));
            }

            for (Object object : pageManager.getRecentlyAddedBlogPosts(20, space.getKey())) {
                if (!(object instanceof BlogPost)) continue;
                BlogPost blog = (BlogPost) object;
                urlList.add(new PageDef(spaceTitle, blog.getTitle(), baseUrl + blog.getUrlPath(), blog.getLastModificationDate(), "weekly", "1"));
            }

            urlList.add(new PageDef(spaceTitle, "URLs to browse the space (" + space.getDisplayTitle() + ")"));

            urlList.add(new PageDef(spaceTitle, "Browse", baseUrl + space.getBrowseUrlPath(), "", "weekly", "0.5"));
            urlList.add(new PageDef(spaceTitle, "Advanced", baseUrl + space.getAdvancedTabUrlPath(), "", "weekly", "0.5"));
            urlList.add(new PageDef(spaceTitle, "Blog", baseUrl + space.getBlogTabUrlPath(), "", "weekly", "0.5"));
            urlList.add(new PageDef(spaceTitle, "Space root", baseUrl + space.getUrlPath(), "", "weekly", "0.5"));
        }

        Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
        contextMap.put("urlList", urlList);
        sitemapxml = VelocityUtils.getRenderedTemplate("sitemapxml.vm", contextMap);

        Map<String, Object> contextMap2 = MacroUtils.defaultVelocityContext();
        robotstxt = VelocityUtils.getRenderedTemplate("robotstxt.vm", contextMap2);
        return INPUT;
    }

    public String doSave() {
        File robotsFile = robots();
        File sitemapFile = sitemap();

        if (!write(robotsFile, robotstxt)) {
            return INPUT;
        }
        if (!write(sitemapFile, sitemapxml)) {
            return INPUT;
        }

        addActionMessage(getText("seo.admin.success"));
        return SUCCESS;
    }

    public String doLicense() {
        return "license";
    }

    private boolean write(File file, String contents) {
        PrintWriter writer = null;
        try {
            //file.getParentFile().mkdirs();
            file.createNewFile();
            writer = new PrintWriter(file, "UTF-8");
            writer.write(contents);
        } catch (FileNotFoundException e) {
            addActionError(getText("Exception while attempting to save: " + e.getMessage()));
            return false;
        } catch (UnsupportedEncodingException e) {
            addActionError(getText("Exception while attempting to save: " + e.getMessage()));
            return false;
        } catch (IOException e) {
            addActionError(getText("Exception while attempting to save: " + e.getMessage()));
            return false;
        } finally {
            IOUtils.closeQuietly(writer);
        }
        return true;
    }

    public String getRobotstxt() {
        return robotstxt;
    }

    public void setRobotstxt(String robotstxt) {
        this.robotstxt = robotstxt;
    }

    public String getSitemapxml() {
        return sitemapxml;
    }

    public void setSitemapxml(String sitemapxml) {
        this.sitemapxml = sitemapxml;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public void setContentPermissionManager(ContentPermissionManager contentPermissionManager) {
        this.contentPermissionManager = contentPermissionManager;
    }

    public void setPermissionManager(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    public String getRobotsFilePath() {
        return robotsFilePath;
    }

    public class PageDef {
        private String url;
        private String lastmod;
        private String changefreq;
        private String priority;
        private String comment;
        private boolean isComment;
        private String spaceTitle;

        public PageDef(String spaceTitle, String comment) {
            this.spaceTitle = spaceTitle;
            this.comment = comment;
            this.isComment = true;
        }

        public PageDef(String spaceTitle, String comment, String url, String lastmod, String changefreq, String priority) {
            this.spaceTitle = spaceTitle;
            this.isComment = false;
            this.comment = comment;
            this.url = url;
            this.lastmod = lastmod;
            this.changefreq = changefreq;
            this.priority = priority;
        }

        public PageDef(String spaceTitle, String comment, String url, Date lastmod, String changefreq, String priority) {
            this.spaceTitle = spaceTitle;
            this.isComment = false;
            this.comment = comment;
            this.url = url;
            String dateAsText = df.format(lastmod);
            this.lastmod = dateAsText.substring(0, 22) + ":" + dateAsText.substring(22);
            this.changefreq = changefreq;
            this.priority = priority;
        }

        public PageDef() {

        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getLastmod() {
            return lastmod;
        }

        public void setLastmod(String lastmod) {
            this.lastmod = lastmod;
        }

        public String getChangefreq() {
            return changefreq;
        }

        public void setChangefreq(String changefreq) {
            this.changefreq = changefreq;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public boolean isComment() {
            return isComment;
        }

        public String getSpaceTitle() {
            return spaceTitle;
        }

        public void setSpaceTitle(String spaceTitle) {
            this.spaceTitle = spaceTitle;
        }
    }

    public void setSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public String getSitemapFilePath() {
        return sitemapFilePath;
    }

    public List<PageDef> getUrlList() {
        return urlList;
    }
}
